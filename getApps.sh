if [ $(netstat -lp 2> /dev/null | grep 3000 | wc -l) -gt 0 ]
then
    echo "Port 3000 already in use by $(netstat -lp 2> /dev/null | grep 3000 | awk '{print $7}')"
    exit
fi

mkdir top-apps 2> /dev/null
mkdir top-apps/apps 2> /dev/null
cd top-apps

read -r -p "Install dependencies? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

    echo "Installing nodejs, jq and git..."
    sudo apt-get install nodejs jq git # jq is used for json processing

    echo "Installing protobuf..."
    sudo pip install protobuf

    echo "Installing nodejs module to scrap app data from google play.."
    npm install google-play-scraper

    echo "Cloning web-api.."
    git clone https://github.com/facundoolano/google-play-api.git web-api

    echo "Cloning python api... (allows downloads)"
    git clone https://github.com/egirault/googleplay-api py-api

    echo "Installing the web-api..."
    cd web-api
    npm install
    cd ..
fi

if [ -z "$ANDROID_ID" ]; then read -r -p "ANDROID ID: " ANDROID_ID; fi
if [ -z "$GOOGLE_LOGIN" ]; then read -r -p "GOOGLE LOGIN: " GOOGLE_LOGIN; fi
unset GOOGLE_PASSWORD
set +a #avoid exporting
read -rsp "GOOGLE PASSWORD: " GOOGLE_PASSWORD
echo ""

echo "import sys" > py-api/config.py
echo "SEPARATOR = \";\"" >> py-api/config.py
echo "LANG = \"en_US\"" >> py-api/config.py
echo "ANDROID_ID = \"$ANDROID_ID\"" >> py-api/config.py
echo "GOOGLE_LOGIN = \"$GOOGLE_LOGIN\"" >> py-api/config.py
echo "GOOGLE_PASSWORD = sys.stdin" >> py-api/config.py
echo "AUTH_TOKEN = None" >> py-api/config.py

cd web-api
npm start & # starts the Node.js web server and respective REST API
WEB_API_PID=$! #stores the process id to kill later
echo "Web-api pid $WEB_API_PID"
cd ..

while [ $(netstat -lp 2> /dev/null | grep 3000 | wc -l) -lt 1 ]
do
   echo "Waiting for web api boot..."
   sleep 1
done

echo "Downloading top apps json..."
wget localhost:3000/api/apps -O top.json

echo "Downloading apps from json..."
cat top.json | jq -r '.results | .[] | .appId' | xargs -I {} sh -c "printf \"%s\n\" \"$GOOGLE_PASSWORD\" | python py-api/download.py {} apps/{}.apk" # printf output does not show on ps

echo "Killing web api..."
kill $WEB_API_PID
echo "Done"
